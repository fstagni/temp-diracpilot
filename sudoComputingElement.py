""" A computing element class that attempts to use sudo
"""

__RCSID__ = "$Id$"

import os
import pwd
import stat
import distutils.spawn

import DIRAC

from DIRAC                                                  import S_OK, S_ERROR

from DIRAC.Resources.Computing.ComputingElement             import ComputingElement
from DIRAC.Core.Utilities.ThreadScheduler                   import gThreadScheduler
from DIRAC.Core.Utilities.Subprocess                        import shellCall

MandatoryParameters = [ ]

class sudoComputingElement( ComputingElement ):

  mandatoryParameters = MandatoryParameters

  #############################################################################
  def __init__( self, ceUniqueID ):
    """ Standard constructor.
    """
    ComputingElement.__init__( self, ceUniqueID )
    self.submittedJobs = 0

  #############################################################################
  def _addCEConfigDefaults( self ):
    """Method to make sure all necessary Configuration Parameters are defined
    """
    # First assure that any global parameters are loaded
    ComputingElement._addCEConfigDefaults( self )
    # Now sudo CE specific ones

  #############################################################################
  def findFreePayloadUser( self ):
    """Pick next free payload user, using exclusive open of .sudolock to
       prevent race conditions if multiple JobAgents/sudoCE running
    """
    
    for i in range(0,100):
      
      userName = os.environ['USER'] + ('%02d' % i)
      userHome = '/scratch/' + userName
      
      try:
#        print os.environ['USER'],userHome,userHome + '/.sudolock'
        fd = os.open( userHome + '/.sudolock', os.O_CREAT | os.O_EXCL )
      except:
        # Payload user already in use
        continue
            
      return userName
      
    # None free now
    return None      
        
  #############################################################################
  def submitJob( self, executableFile, proxy, dummy = None ):
    """ Method to submit job, overridden from super-class.
    """
    self.log.verbose( 'Setting up proxy for payload' )
    result = self.writeProxyToFile( proxy )
    if not result['OK']:
      return result

    payloadProxy = result['Value']
    if not os.environ.has_key( 'X509_USER_PROXY' ):
      self.log.error( 'X509_USER_PROXY variable for pilot proxy not found in local environment' )
      return S_ERROR( 'X509_USER_PROXY not found' )

    pilotProxy = os.environ['X509_USER_PROXY']
    self.log.info( 'Pilot proxy X509_USER_PROXY=%s' % pilotProxy )

    # Pick next free payload user
    payloadUsername = self.findFreePayloadUser()
    
    try:
      payloadUID = pwd.getpwnam(payloadUsername).pw_uid
      payloadGID = pwd.getpwnam(payloadUsername).pw_gid
    except:
      error = S_ERROR( 'User "' + str(payloadUsername) + '" does not exist!' )
      error['Value'] = ( 201, '', '' )
      return error

    self.log.verbose( 'Starting process for monitoring payload proxy' )
    gThreadScheduler.addPeriodicTask( self.proxyCheckPeriod, self.monitorProxy,
                                      taskArgs = ( pilotProxy, payloadProxy, payloadUsername, payloadUID ),
                                      executions = 0, elapsedTime = 0 )

    # Submit job
    self.log.info( 'Changing permissions of executable (%s) to 0755' % executableFile )
    try:
      os.chmod( os.path.abspath( executableFile ), stat.S_IRWXU | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH )
    except Exception, x:
      self.log.error( 'Failed to change permissions of executable to 0755 with exception', 
                      '\n%s' % ( x ) )

    result = self.sudoExecute( os.path.abspath( executableFile ), payloadProxy, payloadUsername, payloadUID, payloadGID )
    if not result['OK']:
      # Need to parse this a bit more???
      self.log.error( 'Failed sudoExecute', result )
      return result

    self.log.debug( 'sudo CE result OK' )
    self.submittedJobs += 1
    return S_OK()

  #############################################################################
  def sudoExecute( self, executableFile, payloadProxy, payloadUsername, payloadUID, payloadGID ):
    """Run sudo with checking of the exit status code.
    """

    # Make use the payload can write via its per-user group
    os.chown( '/scratch/' + os.environ['USER'], -1, payloadGID )
    os.chmod( '/scratch/' + os.environ['USER'], stat.S_IRUSR + stat.S_IWUSR + stat.S_IXUSR + stat.S_IRGRP + stat.S_IWGRP + stat.S_IXGRP + stat.S_ISVTX )

    os.chown( payloadProxy, -1, payloadGID )
    os.chmod( payloadProxy, stat.S_IRUSR + stat.S_IWUSR + stat.S_IRGRP )
          
    result = shellCall( 0, 
                        '/usr/bin/sudo -u %s sh -c "cp -f %s /tmp/x509up_u%d ; chmod 0400 /tmp/x509up_u%d"' % ( payloadUsername,  payloadProxy, payloadUID, payloadUID ),
                        callbackFunction = self.sendOutput )

# Still to do? Change DIRACSYSCONFIG if we do this. Or can we remove host stuff from pilot.cfg somehow?
#
## make a config file for pltNNp00 that uses its proxy
#if [ ! -r /scratch/$USER/payload.cfg ] ; then
# sed "s:/.*host[keycert]*.pem:/tmp/x509up_u$PLT_NN_P00_ID:" /scratch/$USER/pilot.cfg > /scratch/$USER/payload.cfg
# sed -i 's/UseServerCertificate.*=.*yes/UseServerCertificate = no/' /scratch/$USER/payload.cfg
# fi    

    # Run the executable (the wrapper in fact)

    cmd = "/usr/bin/sudo -u %s PATH=$PATH DIRACSYSCONFIG=/scratch/%s/pilot.cfg LD_LIBRARY_PATH=$LD_LIBRARY_PATH PYTHONPATH=$PYTHONPATH X509_USER_PROXY=/tmp/x509up_u%d sh -c '%s'" % ( payloadUsername, os.environ['USER'], payloadUID, executableFile )
    self.log.info( 'CE submission command is: %s' % cmd )

    result = shellCall( 0, cmd, callbackFunction = self.sendOutput )
    if not result['OK']:
      result['Value'] = ( 0, '', '' )
      return result

    resultTuple = result['Value']
    status = resultTuple[0]
    stdOutput = resultTuple[1]
    stdError = resultTuple[2]
    self.log.info( "Status after the sudo execution is %s" % str( status ) )
    if status >=127:
      error = S_ERROR( status )
      error['Value'] = ( status, stdOutput, stdError )
      return error

    return result

  #############################################################################
  def getCEStatus( self ):
    """ Method to return information on running and pending jobs.
    """
    result = S_OK()
    result['SubmittedJobs'] = 0
    result['RunningJobs'] = 0
    result['WaitingJobs'] = 0
    return result

  #############################################################################
  def monitorProxy( self, pilotProxy, payloadProxy, payloadUsername, payloadUID ):
    """ Monitor the payload proxy and renew as necessary.
    """
    retVal = self._monitorProxy( pilotProxy, payloadProxy )
    if not retVal['OK']:
      # Failed to renew the proxy, nothing else to be done
      return retVal

    if not retVal['Value']:
      # No need to renew the proxy, nothing else to be done
      return retVal

    self.log.info( 'Re-executing sudo without arguments to renew payload proxy' )

#    chgrp ${USER}pld $GLEXEC_CLIENT_CERT
#    chmod g+r $GLEXEC_CLIENT_CERT
#    PLT_NN_PLD_ID=`id -u ${USER}pld`
    
    # for DM commands to work, proxy must be owned by pltNNp00
#    sudo -u ${USER}pld sh -c "cp -fp $GLEXEC_CLIENT_CERT /tmp/x509up_u$PLT_NN_PLD_ID ; chmod 0400 /tmp/x509up_u$PLT_NN_PLD_ID"
    result = shellCall( 0, 
                        '/usr/bin/sudo -u %s sh -c "cp -f %s /tmp/x509up_u%d ; chmod 0400 /tmp/x509up_u%d"' 
                        % ( payloadUsername,  payloadProxy, payloadUID, payloadUID ),
                        callbackFunction = self.sendOutput )
    
    return S_OK( 'Proxy checked' )

#EOF#EOF#EOF#EOF#EOF#EOF#EOF#EOF#EOF#EOF#EOF#EOF#EOF#EOF#EOF#EOF#EOF#EOF#EOF#
